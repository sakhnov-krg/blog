<?php

return [
    'adminEmail' => 'admin@example.com',
    'tinymce' => [
        'plugins' => ["pagebreak typograf code advlist autolink lists link charmap print preview anchor searchreplace visualblocks code fullscreen insertdatetime media table contextmenu paste image wordcount media textcolor colorpicker autolink template"],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | forecolor | link unlink media image | removeformat typograf template code",
        'templates' => [
//            ['title' => 'Название', 'description' => 'Описание', 'url' => '#'],
        ],
        'other' => [
            'content_css' => ['/css/bootstrap.css', '/css/site.css', '/css/tinymcefix.css', '/css/temp.css'],
            'image_advtab' => true,
            'extended_valid_elements' => "script[language|type|src|charset]",
            'relative_urls' => false,
            'external_filemanager_path' => "/plugins/filemanager/",
            'filemanager_title' => "Глобальное файловое хранилище",
            'external_plugins' => ["filemanager" => "/plugins/filemanager/plugin.min.js", "typograf" => "/plugins/tinymce.typograf.js"],
        ]
    ]
];
