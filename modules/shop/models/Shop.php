<?php

namespace app\modules\shop\models;

use Yii;
use app\behaviors\CyrillicSlugBehavior;
use mongosoft\file\UploadImageBehavior;

/**
 * This is the model class for table "shop".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $time
 * @property int $cost
 * @property string $image
 * @property string $address
 * @property string $location
 * @property string $description
 * @property int $ord
 */
class Shop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop';
    }

    public function behaviors()
    {
        return [
            [
                'class' => CyrillicSlugBehavior::className(),
                'attribute' => 'name'
            ],
            [
            'class' => UploadImageBehavior::className(),
            'attribute' => 'image',
            'scenarios' => ['insert', 'update'],
            'placeholder' => '@webroot/img/no-photo-750x450.png',
            'path' => '@webroot/uploads/shop',
            'url' => '@web/uploads/shop',
            'thumbs' => [
                'thumb' => ['width' => 750, 'height' => 450],
            ],
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'address', 'location'], 'required'],
            [['ord'], 'integer'],
            [['description'], 'string'],
            [['name', 'slug', 'image', 'address', 'location'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Псевдоним',

            'image' => 'Изображение',
            'address' => 'Адрес',
            'location' => 'Координаты',
            'description' => 'Описание',
            'ord' => 'Порядок',
        ];
    }

    /**
     * @inheritdoc
     * @return ShopQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopQuery(get_called_class());
    }
}
