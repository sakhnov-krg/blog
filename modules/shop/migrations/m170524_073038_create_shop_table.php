<?php

namespace app\modules\shop\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `shop`.
 */
class m170524_073038_create_shop_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shop', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'image' => $this->string(),
            'address' => $this->string()->notNull(),
            'location' => $this->string()->notNull(),
            'description' => $this->text(),
            'ord' => $this->boolean()->defaultValue(0)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shop');
    }
}
