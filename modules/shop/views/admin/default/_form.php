<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-form">

    <?php $form = ActiveForm::begin(['validateOnBlur' => false]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true, 'readonly' => true]) ?>
            <div id="map" style="width: 100%; height: 448px;"></div>
            <script src="http://maps.api.2gis.ru/2.0/loader.js"></script>
            <script type="text/javascript">
                var locationInfo = document.getElementById('shop-location');

                DG.then(function () {
                    var map,
                        marker,
                        loc;

                    loc = locationInfo.value == '' ? [49.797084, 73.091912] : locationInfo.value.split(', ');
                    console.log(loc);

                    map = DG.map('map', {
                        center: loc,
                        zoom: 16,
                        fullscreenControl: false,
                        zoomControl: true,
                    });

                    marker = DG.marker(loc, {
                        draggable: true
                    }).addTo(map);

                    marker.on('drag', function (e) {
                        var lat = e.target._latlng.lat.toFixed(6),
                            lng = e.target._latlng.lng.toFixed(6);

                        console.log(lat + ', ' + lng);
                        locationInfo.value = lat + ', ' + lng;
                    });
                });
            </script>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>
        </div>
    </div>




    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
        'language' => 'ru',
        'clientOptions' => array_merge(Yii::$app->params['tinymce']['other'], [
            'plugins' => Yii::$app->params['tinymce']['plugins'],
            'toolbar' => Yii::$app->params['tinymce']['toolbar'],
            'templates' => Yii::$app->params['tinymce']['templates'],
            'height' => 300,
        ])
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
