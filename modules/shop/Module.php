<?php

namespace app\modules\shop;

/**
 * shop module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\shop\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if(!\Yii::$app->admin->isGuest) {
            \Yii::$app->session->set('filemanager', true);
        }

        // custom initialization code goes here
    }
}
