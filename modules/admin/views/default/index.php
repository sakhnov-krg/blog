<?php
use yii\bootstrap\Nav;
use yii\helpers\Html;

$this->title = 'Админка';
?>
<!--<div class="admin-default-index">-->
<!--    <div class="row">-->
<!--        <div class="col-md-4 col-md-offset-4">-->
<!--            <div class="panel panel-default">-->
<!--                <div class="panel-body">-->
                    <?php
//                    echo Nav::widget([
//                        'options' => ['class' => 'nav nav-pills nav-stacked'],
//                        'items' => [
//                            ['label' => 'Магазины', 'url' => ['/admin/shop/default/index']],
//                        ],
//                        'encodeLabels' => false
//                    ]);
                    ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<div class="row">
    <div class="col-md-4">
        <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-red"><i class="fa fa-shopping-basket"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Магазинов</span>
                <span class="info-box-number"><?= \app\modules\shop\models\Shop::find()->count(); ?></span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>
    <div class="col-md-4">

    </div>
    <div class="col-md-4">

    </div>
</div>
