<?php

/* @var $this \yii\web\View */
?>
<footer class="footer">
    <div class="container">
        <p class="text-center">
            © Александр Сахнов <?= date('Y') ?><br>
            <?= Yii::powered(); ?>
        </p>
    </div>
</footer>