<?php
session_start();
mb_internal_encoding('UTF-8');

if(!isset($_SESSION['filemanager'])) {
	die('Access denied!');
}

$base_url ="";
$upload_dir = '/uploads/source/';
$current_path = '../../uploads/source/';
$thumbs_base_path = '../../uploads/thumbs/';

define('USE_ACCESS_KEYS', FALSE); // TRUE or FALSE
$access_keys = array('myPrivateKey','someoneElseKey');
$MaxSizeUpload = 20;

if ((int)(ini_get('post_max_size')) < $MaxSizeUpload){
	$MaxSizeUpload = (int)(ini_get('post_max_size'));
}

$default_language 	= "ru";
$icon_theme 		= "ico";
$show_folder_size 	= TRUE;
$show_sorting_bar 	= TRUE;
$loading_bar 		= TRUE;
$transliteration 	= TRUE;

$image_max_width  = 0;
$image_max_height = 0;

$image_resizing = FALSE;
$image_resizing_width  = 0;
$image_resizing_height = 0;

$default_view = 0;
$ellipsis_title_after_first_row = TRUE;

$delete_files	 = TRUE;
$create_folders	 = TRUE;
$delete_folders	 = TRUE;
$upload_files	 = TRUE;
$rename_files	 = TRUE;
$rename_folders	 = TRUE;
$duplicate_files = TRUE;
$copy_cut_files	 = TRUE;
$copy_cut_dirs	 = TRUE;

$copy_cut_max_size	 = 100;
$copy_cut_max_count	 = 200;

$ext_img = array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'tiff', 'svg'); //Images
$ext_file = array('doc', 'docx','rtf', 'pdf', 'xls', 'xlsx', 'txt', 'csv','html','xhtml','psd','sql','log','fla','xml','ade','adp','mdb','accdb','ppt','pptx','odt','ots','ott','odb','odg','otp','otg','odf','ods','odp','css','ai'); //Files
$ext_video = array('mov', 'mpeg', 'mp4', 'avi', 'mpg','wma',"flv","webm"); //Video 
$ext_music = array('mp3', 'm4a', 'ac3', 'aiff', 'mid','ogg','wav'); //Audio
$ext_misc = array('zip', 'rar','gz','tar','iso','dmg'); //Archives

$ext = array_merge($ext_img, $ext_file, $ext_misc, $ext_video, $ext_music); //allowed extensions

$aviary_active 	= FALSE;
$aviary_key 	= "dvh8qudbp6yx2bnp";
$aviary_secret	= "m6xaym5q42rpw433";
$aviary_version	= 3;
$aviary_language= 'ru';

$file_number_limit_js = 500;

$hidden_folders = array();
$hidden_files = array('config.php');

$java_upload = FALSE;
$JAVAMaxSizeUpload = 200; //Gb

$fixed_image_creation                   = FALSE; //activate or not the creation of one or more image resized with fixed path from filemanager folder
$fixed_path_from_filemanager            = array('../test/','../test1/'); //fixed path of the image folder from the current position on upload folder
$fixed_image_creation_name_to_prepend   = array('','test_'); //name to prepend on filename
$fixed_image_creation_to_append         = array('_test',''); //name to appendon filename
$fixed_image_creation_width             = array(300,400); //width of image (you can leave empty if you set height)
$fixed_image_creation_height            = array(200,''); //height of image (you can leave empty if you set width)


$relative_image_creation                = FALSE; //activate or not the creation of one or more image resized with relative path from upload folder
$relative_path_from_current_pos         = array('thumb/','thumb/'); //relative path of the image folder from the current position on upload folder
$relative_image_creation_name_to_prepend= array('','test_'); //name to prepend on filename
$relative_image_creation_name_to_append = array('_test',''); //name to append on filename
$relative_image_creation_width          = array(300,400); //width of image (you can leave empty if you set height)
$relative_image_creation_height         = array(200,''); //height of image (you can leave empty if you set width)

?>
